import * as THREE from "three";
import { BufferGeometryUtils } from "three/examples/jsm/utils/BufferGeometryUtils"

function createLineWithShapeFromTrajectory(trajectory: THREE.Vector3[], shape: THREE.Shape, color: number) {    
    const geometries = [];

    const extrudeSettings = { depth: 0, bevelEnabled: false, steps: 1 };

    const extrudedGeometry = new THREE.ExtrudeGeometry(shape, extrudeSettings);
    extrudedGeometry.computeBoundingBox();

    let previousPoint;
    for (let i = 0; i < trajectory.length - 1; i++) {
        let newGeomInstance = extrudedGeometry.clone();
        let subVectors;
        if (previousPoint) {
            subVectors = new THREE.Vector3().subVectors(trajectory[i + 1], previousPoint);
        } else {
            subVectors = new THREE.Vector3().subVectors(trajectory[i], trajectory[i + 1]);
        }
        newGeomInstance.applyMatrix4(
            new THREE.Matrix4().makeRotationZ(Math.atan(subVectors.y / subVectors.x !== NaN ? subVectors.y / subVectors.x : 0)));
        newGeomInstance.applyMatrix4(new THREE.Matrix4().makeTranslation(trajectory[i].x, trajectory[i].y, trajectory[i].z));

        geometries.push(newGeomInstance);
        previousPoint = trajectory[i];
    }

    const meshMaterial = new THREE.MeshBasicMaterial({
        color: color,
        side: THREE.DoubleSide,
        transparent: false,
        depthWrite: false
    });
    const mergedGeom = BufferGeometryUtils.mergeBufferGeometries(geometries.length>1? geometries: [extrudedGeometry]);

    return new THREE.Mesh(mergedGeom, meshMaterial);
}


function createRectangleShape(length: number, height: number) {
    return new THREE.Shape([
        new THREE.Vector2(height / 2, -length / 2),
        new THREE.Vector2(height / 2, length / 2),
        new THREE.Vector2(-height / 2, length / 2),
        new THREE.Vector2(-height / 2, -length / 2),
        new THREE.Vector2(height / 2, -length / 2)
    ]);
}

function normalizeTrajectory(points: THREE.Vector3[], interval: number): THREE.Vector3[] {
    if(points.length>=2){
        const curve = new THREE.CatmullRomCurve3(points);
        return curve.getPoints(curve.getLength() / interval);
    } else {
        return points;
    }
}

function getMeshLine(trajectory: THREE.Vector3[]): THREE.Mesh[] {
    const rectangleBig = createRectangleShape(5, 10);
    const rectangleSmall = createRectangleShape(4, 9);
    const stripMeshObjWithBig = createLineWithShapeFromTrajectory(trajectory, rectangleBig, 0x000000);
    const stripMeshObjWithSmall = createLineWithShapeFromTrajectory(trajectory, rectangleSmall, 0xffffff);
    return [stripMeshObjWithBig, stripMeshObjWithSmall];
}

function getMeshLineNormalized(points: THREE.Vector3[]): THREE.Mesh[] {
    return getMeshLine(normalizeTrajectory(points, 16));
}

export { createLineWithShapeFromTrajectory, getMeshLine, getMeshLineNormalized, createRectangleShape, normalizeTrajectory };