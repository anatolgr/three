import { Component, OnInit } from '@angular/core';
import * as THREE from 'three';
import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls'
import {
  getMeshLineNormalized
} from '../helpers/lines'

@Component({
  selector: 'app-lines',
  templateUrl: './lines.component.html',
  styleUrls: ['./lines.component.css']
})
export class LinesComponent implements OnInit {

  private isRenderRequested = false;
  
  private container: HTMLElement | null = null;

  private renderer: THREE.WebGLRenderer | null = null;

  private scene: THREE.Scene = new THREE.Scene();

  private camera: THREE.PerspectiveCamera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1, 125000);

  private controls: OrbitControls| null = null; 

  constructor() { }

  ngOnInit(): void {
    this.container = document.getElementById("three-container") as HTMLCanvasElement;
    this.renderer = new THREE.WebGLRenderer({canvas: this.container});
    this.renderer.setPixelRatio(window.devicePixelRatio);
    this.renderer.setSize(this.container.clientWidth, this.container.clientHeight);

    this.camera.position.set( 0, 0, 500 );
    this.camera.lookAt( 0, 0, 0 );
    
    this.controls = new OrbitControls(this.camera, this.renderer.domElement);
    this.controls.autoRotate = true;
    this.scene.background = new THREE.Color(255,255,255);
    this.render = this.render.bind(this);

    this.scene.add(...getMeshLineNormalized(this.createTestTrajectory(Math.PI/40, 10)));
    this.scene.add(...getMeshLineNormalized([
      new THREE.Vector3(0,0,0),
      new THREE.Vector3(100,0,0),
      new THREE.Vector3(200,100,0),
      new THREE.Vector3(300,0,0),
      new THREE.Vector3(400,0,0),
    ]));
    this.isRenderRequested = true;
    this.render();
  }

  createTestTrajectory(step:number, curls=10):THREE.Vector3[]{
    let maxRadius = 200;

    //x = cx + r * cos(a)
    //y = cy + r * sin(a)
    const points = [];
    for(let j=0; j<curls; j++){
        for (let i=0; i<Math.PI*2 ; i+=step){
            points.push(new THREE.Vector3(maxRadius*Math.cos(i), maxRadius*Math.sin(i), 0));
        }
        maxRadius-=10;
    }
    return points;
  }

  render(){
    if (this.isRenderRequested){
      this.renderer?.render(this.scene, this.camera);
      this.controls?.update();
      // this.isRenderRequested =false;
    }
    requestAnimationFrame(this.render);
  }

}
